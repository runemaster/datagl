#version 460 core

layout (location = 0) uniform mat4 MVP;
layout (location = 0) in vec2 vertexPos;
out vec2 vertexUV;

void main() {
    vertexUV = vertexPos;
    gl_Position = MVP * vec4(vertexPos, 0.0, 1.0);
}