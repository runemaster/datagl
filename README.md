# datagl

Prototypes and demos of data visualization tools using OpenGL.

## Development

To setup the development environment you will need the following tools:

* C compiler: I use gcc 9.3.0;
* CMake
* Conan
* Ninja (optional)

Run the following commands in order to setup the development environment:

```
$ mkdir -p build && cd build/
$ conan install ..
$ cmake -G "Ninja" ..
$ make
```
