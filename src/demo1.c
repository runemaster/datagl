#include <stdio.h>

#include "gl.h"
#include "linmath.h"
#define GLFW_INCLUDE_NONE
#include "GLFW/glfw3.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include "stb_image.h"
#pragma GCC diagnostic pop

#include "core.h"


// ========================================================
//
//                    Static variables
//
// ========================================================

static GLFWwindow *window = NULL;
static const unsigned int window_width = 1280;
static const unsigned int window_height = 900;
static vec2 camera_pos = {0.f, 0.f};
static const float camera_speed = 50.0f;
static float camera_scale = 1.0f;

static const float unit_square[12] = {
  0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
  1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
};

// ========================================================
//
//                      Callbacks
//
// ========================================================

static void glfw_error_callback(int code, const char *description) {
  printf("GLFW error %i: %s", code, description);
}

static void glfw_key_callback(GLFWwindow *w, int key, int code, int action, int mods) {
  DL_UNUSED(w);
  DL_UNUSED(code);
  DL_UNUSED(mods);

  vec2 camera_movement;
  if(key == GLFW_KEY_A && action == GLFW_PRESS) {
    camera_movement[0] = -camera_speed;
  } else if(key == GLFW_KEY_D && action == GLFW_PRESS) {
    camera_movement[0] = camera_speed;
  } else if(key == GLFW_KEY_W && action == GLFW_PRESS) {
    camera_movement[1] = camera_speed;
  } else if(key == GLFW_KEY_S && action == GLFW_PRESS) {
    camera_movement[1] = -camera_speed;
  } else if(key == GLFW_KEY_KP_ADD && action == GLFW_PRESS) {
    camera_scale += 1.0f;
  }
  vec2_add(camera_pos, camera_pos, camera_movement);
}


// ========================================================
//
//                      Rendering
//
// ========================================================

typedef unsigned int texture_t;
typedef unsigned int shader_t;
typedef unsigned int program_t;
typedef unsigned int buffer_t;
typedef unsigned int vao_t;



int main(int argc, char *argv[]) {
  DL_UNUSED(argc);
  DL_UNUSED(argv);
  DL_UNUSED(unit_square);

  glfwSetErrorCallback(glfw_error_callback);
  glfwInit();

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window = glfwCreateWindow(window_width, window_height, "datagl - demo1", NULL, NULL);
  if(!window) return 1;

  glfwSetKeyCallback(window, glfw_key_callback);
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  gladLoadGL(glfwGetProcAddress);
  glClearColor(0.05f, 0.05f, 0.1f, 1.0f);
  glViewport(0, 0, window_width, window_height);

  // ========================================================
  //
  //                        Texture
  //
  // ========================================================


  int width, height, channels;
  stbi_set_flip_vertically_on_load(1);
  void *data = stbi_load("data/peru.png", &width, &height, &channels, 0);
  if(!data) return 1;

  texture_t data_texture;
  glGenTextures(1, &data_texture);
  glBindTexture(GL_TEXTURE_2D, data_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
  glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
  glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);


  // ========================================================
  //
  //                       Buffers
  //
  // ========================================================

  vao_t vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  buffer_t vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(unit_square), unit_square, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, NULL);
  glEnableVertexAttribArray(0);
  glBindVertexArray(0);


  // ========================================================
  //
  //                       Shaders
  //
  // ========================================================

  shader_t vs, fs;
  program_t program;

  FILE *vfp = fopen("shaders/quad.vert", "r");
  fseek(vfp, 0, SEEK_END);
  long int vsf_size = ftell(vfp);
  rewind(vfp);
  char *vs_source = DL_CALLOC(vsf_size + 1, sizeof(char));
  fread(vs_source, sizeof(char), vsf_size, vfp);
  fclose(vfp);

  vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vs, 1, (const char **)&vs_source, NULL);
  glCompileShader(vs);
  DL_FREE(vs_source);

  int vs_status; glGetShaderiv(vs, GL_COMPILE_STATUS, &vs_status);
  if(!vs_status) {
    char info_log[1024] = {0};
    glGetShaderInfoLog(vs, 1024, NULL, info_log);
    printf("Vertex shader compilation failed\n%s\n", info_log);
  }

  FILE *ffp = fopen("shaders/quad.frag", "r");
  fseek(ffp, 0, SEEK_END);
  long int fsf_size = ftell(ffp);
  rewind(ffp);
  char *fs_source = DL_CALLOC(fsf_size + 1, sizeof(char));
  fread(fs_source, sizeof(char), fsf_size, ffp);
  fclose(ffp);

  fs = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fs, 1, (const char **)&fs_source, NULL);
  glCompileShader(fs);
  DL_FREE(fs_source);

  int fs_status; glGetShaderiv(fs, GL_COMPILE_STATUS, &fs_status);
  if(!fs_status) {
    char info_log[1024] = {0};
    glGetShaderInfoLog(fs, 1024, NULL, info_log);
    printf("Fragment shader compilation failed\n%s\n", info_log);
  }

  program = glCreateProgram();
  glAttachShader(program, vs);
  glAttachShader(program, fs);
  glLinkProgram(program);

  int program_status; glGetProgramiv(program, GL_LINK_STATUS, &program_status);
  if(!program_status) {
    char info_log[1024] = {0};
    glGetProgramInfoLog(program, 1024, NULL, info_log);
    printf("Program link failed\n%s\n", info_log);
  }

  glDetachShader(program, vs); glDeleteShader(vs);
  glDetachShader(program, fs); glDeleteShader(fs);


  // ========================================================
  //
  //                      Main loop
  //
  // ========================================================

  while(!glfwWindowShouldClose(window)) {
    glfwPollEvents();

    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    mat4x4 mvp, p, i, v, m;
    mat4x4_identity(mvp);
    mat4x4_identity(m);
    mat4x4_identity(v);
    mat4x4_identity(i);
    mat4x4_identity(p);

    mat4x4_scale_aniso(m, m, width*camera_scale, height*camera_scale, 0.0f);
    mat4x4_translate(v, camera_pos[0], camera_pos[1], 0.f);
    mat4x4_ortho(p, 0, window_width, 0, window_height, -1, 1);
    
    mat4x4_mul(i, v, m);
    mat4x4_mul(mvp, p, i);
    glUniformMatrix4fv(0, 1, GL_FALSE, (const float*)&mvp);

    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D, data_texture);
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, 6);
  
    glfwSwapBuffers(window);
  }


  // ========================================================
  //
  //                   Clear resources
  //
  // ========================================================

  glDeleteBuffers(1, &vbo);
  glDeleteVertexArrays(1, &vao);
  glDeleteTextures(1, &data_texture);
  glfwDestroyWindow(window);
  glfwTerminate();

  return 0;
}