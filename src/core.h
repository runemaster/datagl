#ifndef DATAGL_CORE_H_
#define DATAGL_CORE_H_

// ========================================================
//
//                    Standard library
//
// ========================================================

#include <stdlib.h>
#include <string.h>


// ========================================================
//
//                    Memory management
//
// ========================================================

#define DL_MALLOC malloc
#define DL_CALLOC calloc
#define DL_REALLOC realloc
#define DL_FREE free
#define DL_MEMSET memset
#define DL_MEMCPY memcpy


// ========================================================
//
//                     Utility macros
//
// ========================================================

#define DL_UNUSED(x) (void)(x)

#endif