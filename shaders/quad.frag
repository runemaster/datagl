#version 460 core

layout (binding = 0) uniform sampler2D dataTexture;
in vec2 vertexUV;
out vec4 fragColor;

void main() {
    fragColor = vec4(
        texture(dataTexture, vertexUV).r,
        0.0,
        0.0,
        1.0
    );
}